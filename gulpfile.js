const gulp = require('gulp');

const pug = require('gulp-pug');
const sass = require("gulp-sass");
const watch = require("gulp-watch");
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const connect = require('gulp-connect');
const uglify = require('gulp-uglify');
const merge = require('merge-stream');
const usemin = require('gulp-usemin');
const cleanCSS = require('gulp-clean-css')

const DIST_DIR = '.';
const SRC_DIR = 'src';

function showError(error) {
  console.log(error.toString());
  this.emit('end');
}

gulp.task('connect', ()=>{
  connect.server({
    root: DIST_DIR,
    port: 8001,
    livereload: true,
    fallback: 'index.html'
  });
});

gulp.task('js', ()=>{
  return gulp.src(`${SRC_DIR}/assets/javascripts/**/*.js`)
    .pipe(uglify())
    .on('error', showError)
    .pipe(gulp.dest(`${DIST_DIR}/assets/javascripts`))
		.pipe(connect.reload());
});

gulp.task('concat', ()=>{
  return gulp.src(`${DIST_DIR}/assets/javascripts/**/*.js`)
    .pipe(concat('app.min.js'))
    .on('error', showError)
    .pipe(gulp.dest(`${DIST_DIR}/assets/javascripts`));
});

gulp.task('html', ()=>{
  let base = gulp.src(`${SRC_DIR}/assets/views/*.pug`)
		.pipe(pug({ pretty: true }))
    .on('error', showError)
		.pipe(gulp.dest(DIST_DIR))
		.pipe(connect.reload());

  let partials = gulp.src(`${SRC_DIR}/assets/views/partials/*.pug`)
		.pipe(pug({ pretty: true }))
    .on('error', showError)
		.pipe(gulp.dest(`${DIST_DIR}/partials/`))
		.pipe(connect.reload());

  return merge(base, partials);
});

gulp.task('css', ()=>{
  return gulp.src(`${SRC_DIR}/assets/stylesheets/*.scss`)
		.pipe(sourcemaps.init())
		.pipe(sass())
    .on('error', showError)
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(`${DIST_DIR}/assets/stylesheets`))
		.pipe(connect.reload());
});

gulp.task('stream', ()=> {
  gulp.watch(`${SRC_DIR}/assets/views/**/*.pug`, ['html']);
  gulp.watch(`${SRC_DIR}/assets/javascripts/**/*.js`, ['js']);
  gulp.watch(`${SRC_DIR}/assets/stylesheets/**/*.scss`, ['css']);
});

gulp.task('usemin', function() {
  return gulp.src('./index.html')
    .pipe(usemin({
    	js: [ uglify() ],
    	css: [ cleanCSS() ]
    }))
    .pipe(gulp.dest(DIST_DIR));
});

gulp.task('watch', ['css', 'js', 'html', 'stream', 'connect']);
gulp.task('build', ['usemin']);
gulp.task('default', ['html', 'css']);
