( function() {
	'use strict';

	angular
		.module( 'webComics' )
		.controller( 'listController', listController );

	listController.$inject = [
		'$scope', '$rootScope', 'comicsService', '$state', '$stateParams', '$mdToast'
	];

	function listController(
		$scope, $rootScope, comicsService, $state, $stateParams, $mdToast
	) {
		var vm = this;
		vm.fetchCommics = fetchCommics;
		vm.selectedItem = selectedItem;
		vm.loadMore = loadMore;

		$rootScope.showComic = false;
		$rootScope.nameBar = 'WEB COMICS' ;

		if($stateParams.msg){
			$mdToast.show(
			  $mdToast.simple()
			    .textContent($stateParams.msg)
			    .hideDelay(3000)
			);
		};


		function fetchCommics(searchText) {
			return comicsService.fetchComics(searchText)
				.then(function(response) {
					return response.data.results;
				});
		}

		function selectedItem( comic ){
			$state.go( 'comic', { comicId: comic.id, comic: comic } );
		}

		function loadMore(){
			var conf = $rootScope.configs;
			$rootScope.proccecingMore = true;

			if( $rootScope.fecthing ){ return false; }

			$rootScope.fecthing = true;

			comicsService.getComics(conf)
				.then(function(response){
					$rootScope.configs.offset += response.data.count;

					response.data.results.map(function(ITEM){

						var checkIsFavorite = JSON.parse(localStorage.UserFavorited)
							.some(function(FAVORITE){
								return FAVORITE.id === ITEM.id;
							});

						ITEM.favorited = checkIsFavorite;
						$rootScope.commics.push( ITEM );
					});
					$rootScope.proccecingMore = false;
					$rootScope.fecthing = false;
				});
		}
	}
} )();
