( function() {
	'use strict';

	angular
		.module( 'webComics', [
			'ngAnimate', 'ngAria', 'ngMaterial', 'ui.router', 'infinite-scroll'
		] )
		.config( [
				'$mdThemingProvider', '$stateProvider', '$urlRouterProvider',
				'$locationProvider',
			function(
				$mdThemingProvider,
				$stateProvider,
				$urlRouterProvider,
				$locationProvider
			) {
				$mdThemingProvider.theme( 'default' )
					.primaryPalette( 'grey', {
						'default': '900'
					} );

				$urlRouterProvider.otherwise( '/' );

				$stateProvider
					.state('home', {
						url: '/',
						templateUrl: './partials/partial-list.html',
						controller: 'listController as vm',
						params: {
							msg: null
						}
					})
					.state('comic', {
						url: '/comic/:comicId',
						templateUrl: './partials/partial-item.html',
						controller: 'ItemController as it',
						params: {
							comicId: null
						}
					})
					.state('favorites', {
						url: '/favorites',
						templateUrl: './partials/partial-list-favorites.html',
						controller: 'favoritesController as fv'
					});

					$locationProvider.html5Mode( true ).hashPrefix( '!' )
			}
		] )
		.run( [
			'$rootScope', '$http', '$transitions', '$state',
			'$stateParams',
			function(
				$rootScope, $http, $transitions , $state, $stateParams
			){
				!localStorage.UserFavorited &&
				localStorage.setItem('UserFavorited', JSON.stringify([]))

				$rootScope.favoritedCount = JSON.parse(localStorage.UserFavorited).length;
				$rootScope.colorHeart = '#e0245e';
			}
		] )
} )();
