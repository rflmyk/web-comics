( function() {
	'use strict';

	angular
		.module( 'webComics' )
		.controller( 'ItemController', ItemController );

	ItemController.$inject = [
		'$scope', '$rootScope', 'comicsService', '$stateParams', '$state'
	];

	function ItemController(
		$scope, $rootScope, comicsService, $stateParams, $state
	){
		var vm = this;
		vm.toggleFromListFavorites = toggleFromListFavorites;
		$rootScope.proccecing = true;
		$rootScope.showComic =  true;
		vm.urlTwitter = 'https://twitter.com/intent/tweet?url=' + location.href;
		vm.urlFacebook = 'https://www.facebook.com/sharer.php?u=' + location.href;

		if( $stateParams.comic ){
			$rootScope.proccecing = false;
			vm.currentItem = $stateParams.comic;
		}

    !$stateParams.comic &&
    comicsService.getComic($stateParams.comicId)
    	.then(function(response){
    		var currentItem = response.data.results[0];
				var checkIsFavorite = JSON.parse( localStorage.UserFavorited )
					.some( function( FAVORITE ){
						return FAVORITE.id === currentItem.id;
					});

				currentItem.favorited = checkIsFavorite;
    		vm.currentItem = currentItem;

    		currentItem.characters &&
    		currentItem.characters.available &&
    		currentItem.characters.items.map(function( resources ){
					comicsService.getGeneric(resources.resourceURI).then(function(response){
						var data = response.data.results[0];
						resources.photo = data.thumbnail.path +'.'+ data.thumbnail.extension;
						resources.desc = data.description;
					});
    		});

				$rootScope.proccecing = false;
    		$rootScope.nameBar = vm.currentItem.title;

    	})
    	.catch(function(error){
  			$state.go( 'home', { msg: 'Something wrong happened :(' } );
    		console.log(error);
    	});

    function toggleFromListFavorites( comic ){
    	comicsService.favorite( comic );

    	$rootScope.commics.map( function(ELEM){
    		if( comic.id === ELEM.id ){
    			if(ELEM.favorited){
    				ELEM.favorited = !ELEM.favorited;
    			}
    		}
    	});
    }
	}
} )();
