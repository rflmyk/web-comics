( function() {
	'use strict';
	
	angular
		.module('webComics')
		.service('comicsService', comicsService);

	comicsService.$inject = ['$http', '$rootScope'];

	function comicsService($http, $rootScope) {
		return {
			getComics: getComics,
			getComic: getComic,
			getGeneric: getGeneric,
			fetchComics: fetchComics,
			favorite: favorite,
			removeFavorite: removeFavorite
		};

		function getComics(param) {
			var url = 'https://gateway.marvel.com/v1/public/comics?orderBy=title&format=comic';

			if(param){
				url = 'https://gateway.marvel.com/v1/public/comics?orderBy=title&format=comic&offset=' + param.offset;
			}

			return $http({
						url: url,
						method: "GET",
						params: {apikey: 'c5bafdbf0af881f12be561c5c08205e0'}
				 })
				.then(getComicsComplete)
				.catch(getComicsFailed);

			function getComicsComplete(response) {
				return response.data;
			}

			function getComicsFailed(error) {
				console.error('XHR Failed for getComics.' + error.data);
			}
		}

		function getComic(commicId){
			return $http({
						url: 'http://gateway.marvel.com/v1/public/comics/' + commicId,
						method: "GET",
						params: {
							apikey: 'c5bafdbf0af881f12be561c5c08205e0'
						}
				 })
				.then(getComicComplete)
				.catch(getComicFailed);

			function getComicComplete(response) {
				return response.data;
			}

			function getComicFailed(error) {
				console.error('XHR Failed for getComic.' + error.status);
			}
		}

		function fetchComics(title){
			return $http({
						url: 'http://gateway.marvel.com/v1/public/comics?orderBy=title&format=comic&titleStartsWith=' + title,
						method: "GET",
						params: {
							apikey: 'c5bafdbf0af881f12be561c5c08205e0'
						}
				 })
				.then(fetchComicComplete)
				.catch(fetchComicFailed);

			function fetchComicComplete(response) {
				return response.data;
			}

			function fetchComicFailed(error) {
				console.error('XHR Failed for fetchComics.' + error.data);
			}
		}

		function getGeneric(url){
			return $http({
						url: url,
						method: "GET",
						params: {
							apikey: 'c5bafdbf0af881f12be561c5c08205e0'
						}
				 })
				.then(getGenericComplete)
				.catch(getGenericFailed);

			function getGenericComplete(response) {
				return response.data;
			}

			function getGenericFailed(error) {
				console.error('XHR Failed for getComic.' + error.status);
			}
		}

		function favorite(commic){
			var currentOrder = commic;
			var favoritedByUser = JSON.parse( localStorage.getItem('UserFavorited') );
			console.log(commic);
			commic.favorited = !commic.favorited;

			if(favoritedByUser){
				var checkItemInStorage = favoritedByUser.some(function (el) {
    			return el.id === currentOrder.id;
  			});

  			if( checkItemInStorage ){
					favoritedByUser = favoritedByUser.filter( function(el){
						 return el.id !== currentOrder.id
					});
  			} else {
  				favoritedByUser.push(currentOrder);
  			}

  			localStorage.setItem('UserFavorited', JSON.stringify(favoritedByUser));
			} else {
				localStorage.setItem('UserFavorited', JSON.stringify([currentOrder]));
			}
			$rootScope.favoritedCount = JSON.parse( localStorage.getItem('UserFavorited') ).length ;
		}

		function removeFavorite(comicId){
			var favoritedByUser = JSON.parse( localStorage.getItem('UserFavorited') );

			favoritedByUser = favoritedByUser.filter( function(el){
				 return el.id !== comicId;
			});

			$rootScope.favoritedCount = JSON.parse( localStorage.getItem('UserFavorited') ).length ;
			localStorage.setItem('UserFavorited', JSON.stringify(favoritedByUser));
		}
	}
} )();
