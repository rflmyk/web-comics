( function(){
	'use strict';

	angular
		.module( 'webComics' )
		.controller( 'mainController', mainController );

	mainController.$inject = [
		'$scope', '$rootScope', 'comicsService', '$state', '$stateParams'
	];

	function mainController(
		$scope, $rootScope, comicsService, $state, $stateParams
	) {
		var vm = this;
		$rootScope.favorite = favorite;
		vm.backToHome = backToHome;
		vm.goToFavorite = goToFavorite;

		$rootScope.showDetails = showDetails;
		$rootScope.showComic =  false;

		$rootScope.proccecing = true;
		$rootScope.fecthing = true;

		comicsService.getComics()
			.then(function(response){
				$rootScope.configs = {
					count: response.data.count,
					limit: response.data.limit,
					offset: response.data.offset + response.data.count
				};

				$rootScope.commics = response.data.results.map(function(ITEM){

					var checkIsFavorite = JSON.parse(localStorage.UserFavorited)
						.some(function(FAVORITE){
							return FAVORITE.id === ITEM.id;
						});

					ITEM.favorited = checkIsFavorite;
					return ITEM
				});

				$rootScope.proccecing = false;
				$rootScope.fecthing = false;
			});

		function favorite( commic ){ return comicsService.favorite(commic) }

		function backToHome(){ $state.go( 'home' ); }

		function goToFavorite(){ $state.go( 'favorites' ); }

		function showDetails(COMIC_ID){
			$state.go( 'comic', { comicId: COMIC_ID } );
		}
	}
} )();
