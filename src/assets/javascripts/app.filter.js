( function() {
	'use strict';

	angular
		.module('webComics')
		.filter('html', html);

	html.$inject = ['$sce'];

	function html($sce) {
		return function( txt ){
			return $sce.trustAsHtml( txt );
		}
	}
} )();
