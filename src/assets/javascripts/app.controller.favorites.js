( function() {
	'use strict';

	angular
		.module( 'webComics' )
		.controller( 'favoritesController', favoritesController );

	favoritesController.$inject = [
		'$scope', '$rootScope', 'comicsService', '$state', '$stateParams'
	];

	function favoritesController(
		$scope, $rootScope, comicsService, $state, $stateParams
	){
		$rootScope.showComic =  true;
		$rootScope.nameBar = 'MY FAVORITES';

		var vm = this;
		vm.favoritesComics = JSON.parse( localStorage.getItem('UserFavorited') );
		vm.removeFavorite = removeFavorite;

		function checkHasFavorites(){
			!vm.favoritesComics.length &&
				$state.go( 'home', { msg: 'You have no favorites.' } );
		}

		checkHasFavorites();

		function removeFavorite( ComicId ){
			comicsService.removeFavorite( ComicId );

			vm.favoritesComics = JSON.parse( localStorage.getItem('UserFavorited') );

			$rootScope.favoritedCount = vm.favoritesComics.length;
			$rootScope.commics = $rootScope.commics.map(function( item ){
				item.favorited = ( item.id === ComicId ? false : item.favorited );
				return item;
			});

			checkHasFavorites();
		}
	}
} )();
